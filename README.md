# Coding Challenge Backend

Created by Arne Zelasko

## Technologies
* Java 11
* Spring Boot 2.2.6
* Spring Data
* H2 Database
* Freemarker Template Engine
* JQuery
* CanvasJS

## How to start
* Run "mvn clean package"
* go to subfolder "target"
* run "java -jar coding-challenge-backend-0.0.1-SNAPSHOT.jar"
* Open "http://localhost:8080/urlRequests" in Browser (Tested with Chrome 81.0.4044.122)
* Url for requests can be changed by property "monitor.request.url". Default is "http://www.google.de"
* Interval for requests can be changed by property "monitor.request.interval". Default is 5000 


## Implementing Approach
* Generate Application with "https://start.spring.io/". Choose modules for web,spring data,freemarker
* Add H2 database as dependency and setup properties in application.properties
* Implement frontend dummy with static data
* Implement logic for making requests to configurable url
* Implement Scheduler for continously executing requests
* Implement logic for saving requests
* Implement RestController for getting Model for executed requests
* Replace static data in frontend with ajax call to rest controller
* Manual test whole application
* Writing tests( yes this should be done before implementation)

## Logic for create average time of executed requests
* Use method "findAllOrderByRequestTime" in  UrlRequestRepository to get all requests ordered by request time
* Calculate minimum and maximum value of all requests.
* Define variable currentTimeFrame and set value to minimum request time
* While currentTimeFrame is before maximum request time calculate average response time of interval between currentTimeFrame 
  and currentTimeFrame+intervalInSeconds(given parameter) and add value to a list. Set currentTimeFrame to currentTimeFrame + intervalInSeconds.
* return created list
* Note: average of time intervals should be calculated directly at the database(see TODO)

## TODO Backend
* Replace in-memory database with disk-based database
* Create sql scripts for used tables and use Flyway to run them at application start
* Write test for class "UrlRequestExecutor" with mocked http server.Use for eample wiremock library for creating mocked http server.
* Find way to getting average of time intervals directly from the database instead of generating values in memory
* Improve Error handling for failed requests(logging, maybe save failed requests)

## TODO Frontend
* Add sources for jquery and canvas.js to project instead of using cdn links
* move javascript logic to separate files
* move css to separate files


