package de.arnezelasko.codingchallengebackend;

import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequest;
import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequestExecutor;
import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequestService;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MonitoringService {

  private final URL url;

  private final UrlRequestService urlRequestService;

  private final UrlRequestExecutor urlRequestExecutor;

  @Autowired
  public MonitoringService(
      @Value("${monitor.request.url}") String monitorRequestUrl,
      UrlRequestService urlRequestService,
      UrlRequestExecutor urlRequestExecutor)
      throws MalformedURLException {
    this.url = new URL(monitorRequestUrl);
    this.urlRequestService = urlRequestService;
    this.urlRequestExecutor = urlRequestExecutor;
  }

  public void monitorUrlRequest() throws URISyntaxException {
    LocalDateTime requestTime = LocalDateTime.now();
    urlRequestExecutor.executeUrlRequest(url);
    LocalDateTime responseTime = LocalDateTime.now();
    UrlRequest urlRequest = new UrlRequest(url, requestTime, responseTime);
    urlRequestService.saveUrlRequest(urlRequest);
  }
}
