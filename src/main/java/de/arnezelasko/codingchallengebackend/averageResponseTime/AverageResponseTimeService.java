package de.arnezelasko.codingchallengebackend.averageResponseTime;

import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequest;
import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequestService;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AverageResponseTimeService {

  private final UrlRequestService urlRequestService;

  @Autowired
  public AverageResponseTimeService(UrlRequestService urlRequestService) {
    this.urlRequestService = urlRequestService;
  }

  public List<AverageResponseTime> getAverageResponseTimesInMiliSecondsOrderByRequestTime(
      long intervalInSeconds) {
    if (intervalInSeconds <= 0) {
      throw new IllegalArgumentException("intervalInSeconds must be greater then 0");
    }
    List<UrlRequest> urlRequests = urlRequestService.findAllOrderByRequestTime();
    if (urlRequests.isEmpty()) {
      return new ArrayList<>();
    }
    List<AverageResponseTime> averageResponseTimes = new ArrayList<>();
    LocalDateTime timeFrameStart = getMinRequestTime(urlRequests);
    LocalDateTime maxRequestTime = getMaxRequestTime(urlRequests);
    while (timeFrameStart.isBefore(maxRequestTime)) {
      OptionalDouble averageResponseTimeOptional =
          getAverageResponseTimeInMiliSeconds(urlRequests, timeFrameStart, intervalInSeconds);
      if (averageResponseTimeOptional.isPresent()) {
        averageResponseTimes.add(
            new AverageResponseTime(
                timeFrameStart,
                timeFrameStart.plusSeconds(intervalInSeconds),
                averageResponseTimeOptional.getAsDouble()));
      }
      timeFrameStart = timeFrameStart.plusSeconds(intervalInSeconds);
    }
    return averageResponseTimes;
  }

  private LocalDateTime getMaxRequestTime(Collection<UrlRequest> urlRequests) {
    return urlRequests.stream().map(UrlRequest::getRequestTime).max(LocalDateTime::compareTo).get();
  }

  private LocalDateTime getMinRequestTime(Collection<UrlRequest> urlRequests) {
    return urlRequests.stream().map(UrlRequest::getRequestTime).min(LocalDateTime::compareTo).get();
  }

  private OptionalDouble getAverageResponseTimeInMiliSeconds(
      Collection<UrlRequest> urlRequests, LocalDateTime timeFrameStart, long interValInSeconds) {
    Stream<UrlRequest> urlRequestsInIntervalStream =
        urlRequests.stream()
            .filter(
                urlRequest ->
                    isInInteval(urlRequest.getRequestTime(), timeFrameStart, interValInSeconds));
    return urlRequestsInIntervalStream
        .mapToLong(urlRequest -> urlRequest.getResponseTimeInMiliSeconds())
        .average();
  }

  private boolean isInInteval(
      LocalDateTime urlRequestTime, LocalDateTime timeFrameStart, long interValInSeconds) {
    return isAfterOrSame(urlRequestTime, timeFrameStart)
        && isBeforeOrSame(urlRequestTime, timeFrameStart.plusSeconds(interValInSeconds));
  }

  private boolean isAfterOrSame(LocalDateTime localDateTime, LocalDateTime afterorSame) {
    return localDateTime.isAfter(afterorSame) || localDateTime.isEqual(afterorSame);
  }

  private boolean isBeforeOrSame(LocalDateTime localDateTime, LocalDateTime afterorSame) {
    return localDateTime.isBefore(afterorSame) || localDateTime.isEqual(afterorSame);
  }
}
