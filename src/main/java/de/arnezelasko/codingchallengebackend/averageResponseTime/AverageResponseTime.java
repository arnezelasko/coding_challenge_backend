package de.arnezelasko.codingchallengebackend.averageResponseTime;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.LocalDateTime;
import lombok.Getter;

@Getter
public class AverageResponseTime {

  private final LocalDateTime timeFrameStart;

  private final LocalDateTime timeFrameEnd;

  private final String timeFrameLabel;

  private final double averageResponseTime;

  public AverageResponseTime(
      LocalDateTime timeFrameStart, LocalDateTime timeFrameEnd, double averageResponseTime) {
    this.timeFrameStart = timeFrameStart;
    this.timeFrameEnd = timeFrameEnd;
    this.timeFrameLabel = createTimeFrameLabel(timeFrameStart, timeFrameEnd);
    this.averageResponseTime = averageResponseTime;
  }

  private String createTimeFrameLabel(LocalDateTime timeFrameStart, LocalDateTime timeFrameEnd) {
    return timeFrameStart.format(ofPattern("HH:mm:ss"))
        + "-"
        + timeFrameEnd.format(ofPattern("HH:mm:ss"));
  }
}
