package de.arnezelasko.codingchallengebackend.averageResponseTime;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AverageResponseTimeController {

  private final AverageResponseTimeService averageResponseTimeService;

  @Autowired
  public AverageResponseTimeController(AverageResponseTimeService averageResponseTimeService) {
    this.averageResponseTimeService = averageResponseTimeService;
  }

  @GetMapping(value = "/averageResponseTimes")
  public Set<AverageResponseTime> getAverageResponseTimes(
      @RequestParam long intervallInSeconds, @RequestParam long maxItemCount) {
    log.info(
        "Get Average Response Time for intervallInSeconds '{}' and maxItemCount '{}'",
        intervallInSeconds,
        maxItemCount);
    // TODO: limit count of elements by database call not after database call
    return averageResponseTimeService
        .getAverageResponseTimesInMiliSecondsOrderByRequestTime(intervallInSeconds).stream()
        .sorted(Comparator.comparing(AverageResponseTime::getTimeFrameStart))
        .limit(maxItemCount)
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }
}
