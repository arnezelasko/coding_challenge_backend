package de.arnezelasko.codingchallengebackend.urlRequest;

import java.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UrlRequestService {

  private final UrlRequestRepository urlRequestRepository;

  @Autowired
  public UrlRequestService(UrlRequestRepository urlRequestRepository) {
    this.urlRequestRepository = urlRequestRepository;
  }

  public void saveUrlRequest(UrlRequest urlRequest) {
    log.info("Saving UrlRequest '{}'", urlRequest);
    urlRequestRepository.save(urlRequest);
  }

  public List<UrlRequest> findAllOrderByRequestTime() {
    return urlRequestRepository.findAllOrderByRequestTime();
  }
}
