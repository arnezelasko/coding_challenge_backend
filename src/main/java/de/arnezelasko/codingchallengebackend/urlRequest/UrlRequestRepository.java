package de.arnezelasko.codingchallengebackend.urlRequest;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
interface UrlRequestRepository extends JpaRepository<UrlRequest, Long> {

  @Query("Select urlRequest from UrlRequest urlRequest order by requestTime")
  public List<UrlRequest> findAllOrderByRequestTime();
}
