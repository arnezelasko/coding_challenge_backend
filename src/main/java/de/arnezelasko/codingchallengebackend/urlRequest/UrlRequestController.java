package de.arnezelasko.codingchallengebackend.urlRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UrlRequestController {

  @GetMapping(value = "/urlRequests")
  public String index() {
    return "index";
  }
}
