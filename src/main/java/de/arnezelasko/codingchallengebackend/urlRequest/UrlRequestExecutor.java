package de.arnezelasko.codingchallengebackend.urlRequest;

import static java.net.http.HttpRequest.newBuilder;
import static java.net.http.HttpResponse.BodyHandlers.ofString;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UrlRequestExecutor {

  private final HttpClient httpClient;

  @Autowired
  public UrlRequestExecutor() {
    this.httpClient = HttpClient.newHttpClient();
  }

  public void executeUrlRequest(URL url) throws URISyntaxException {
    log.info("Execute Request to '{}'", url.toString());
    final HttpRequest httpRequest = newBuilder(url.toURI()).GET().build();
    try {
      httpClient.send(httpRequest, ofString());
    } catch (IOException | InterruptedException e) {
      log.error("Error while sending request:", e);
    }
  }
}
