package de.arnezelasko.codingchallengebackend.urlRequest;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;

@Getter
@Entity
public class UrlRequest {

  @Id @GeneratedValue private long requestId;

  private URL url;

  private LocalDateTime requestTime;

  private LocalDateTime responseTime;

  public UrlRequest(URL url, LocalDateTime requestTime, LocalDateTime responseTime) {
    this.url = url;
    this.requestTime = requestTime;
    this.responseTime = responseTime;
  }

  // for JPA
  UrlRequest() {}

  public long getResponseTimeInMiliSeconds() {
    return requestTime.until(responseTime, ChronoUnit.MILLIS);
  }

  @Override
  public String toString() {
    return new org.apache.commons.lang3.builder.ToStringBuilder(this)
        .append("requestId", requestId)
        .append("url", url)
        .append("requestTime", requestTime)
        .append("responseTime", responseTime)
        .toString();
  }
}
