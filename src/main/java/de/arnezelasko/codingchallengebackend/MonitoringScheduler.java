package de.arnezelasko.codingchallengebackend;

import java.net.URISyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MonitoringScheduler {

  private final MonitoringService monitoringService;

  public MonitoringScheduler(MonitoringService monitoringService) {
    this.monitoringService = monitoringService;
  }

  @Scheduled(fixedDelayString = "${monitor.request.interval}")
  public void monitorUrl() throws URISyntaxException {
    monitoringService.monitorUrlRequest();
  }
}
