package de.arnezelasko.codingchallengebackend.urlRequest;

import static java.time.LocalDateTime.of;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UrlRequestTest {

  private UrlRequest urlRequestUnderTest;

  @BeforeEach
  void setUp() throws MalformedURLException {
    LocalDateTime requestTime = of(2017, 1, 1, 0, 0, 0);
    int requestDurationInSeconds = 1;
    LocalDateTime responseTime = requestTime.plusSeconds(requestDurationInSeconds);
    urlRequestUnderTest = new UrlRequest(new URL("http://example.com/"), requestTime, responseTime);
  }

  @Test
  void testGetResponseTimeInMiliSeconds() {
    // Setup

    // Run the test
    final long result = urlRequestUnderTest.getResponseTimeInMiliSeconds();

    // Verify the results
    assertEquals(1000L, result);
  }
}
