package de.arnezelasko.codingchallengebackend.urlRequest;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UrlRequestRepositoryTest {

  @Autowired private UrlRequestRepository urlRequestRepository;

  @Test
  public void findAllOrderByRequestTime() throws MalformedURLException {
    // given
    UrlRequest urlRequestNow = givenUrlRequest(now());
    UrlRequest urlRequestFuture = givenUrlRequest(urlRequestNow.getRequestTime().plusSeconds(1));
    urlRequestRepository.save(urlRequestNow);
    urlRequestRepository.save(urlRequestFuture);

    // when
    List<UrlRequest> urlRequestsOrderedByRequestTime =
        urlRequestRepository.findAllOrderByRequestTime();

    // then
    assertThat(urlRequestsOrderedByRequestTime.size()).isEqualTo(2);
    assertThat(urlRequestsOrderedByRequestTime.get(0).getRequestTime())
        .isEqualTo(urlRequestNow.getRequestTime());
    assertThat(urlRequestsOrderedByRequestTime.get(1).getRequestTime())
        .isEqualTo(urlRequestFuture.getRequestTime());
  }

  private UrlRequest givenUrlRequest(LocalDateTime requestTime) throws MalformedURLException {
    return new UrlRequest(new URL("http://example.com/"), requestTime, requestTime.plusSeconds(1));
  }
}
