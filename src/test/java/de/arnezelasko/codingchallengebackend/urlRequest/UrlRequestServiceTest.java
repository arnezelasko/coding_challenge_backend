package de.arnezelasko.codingchallengebackend.urlRequest;

import static de.arnezelasko.codingchallengebackend.EasyRandomUtil.getRandomInstance;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

class UrlRequestServiceTest {

  @Mock private UrlRequestRepository urlRequestRepository;

  private UrlRequestService urlRequestService;

  @BeforeEach
  void setUp() {
    initMocks(this);
    urlRequestService = new UrlRequestService(urlRequestRepository);
  }

  @Test
  void testSaveUrlRequest() throws Exception {
    // given
    final UrlRequest urlRequest = createUrlRequest();

    // when
    urlRequestService.saveUrlRequest(urlRequest);

    // then
    verify(urlRequestRepository).save(urlRequest);
  }

  @Test
  void testFindAllOrderByRequestTime() throws Exception {
    // given
    UrlRequest urlRequest = createUrlRequest();
    List<UrlRequest> urlRequests = new ArrayList<>();
    urlRequests.add(urlRequest);
    givenUrlRequestsInDatabase(urlRequests);

    // when
    final List<UrlRequest> result = urlRequestService.findAllOrderByRequestTime();

    // then
    assertThat(result).isEqualTo(urlRequests);
  }

  private void givenUrlRequestsInDatabase(List<UrlRequest> urlRequests) {
    when(urlRequestRepository.findAllOrderByRequestTime()).thenReturn(urlRequests);
  }

  private UrlRequest createUrlRequest() {
    return getRandomInstance(UrlRequest.class);
  }
}
