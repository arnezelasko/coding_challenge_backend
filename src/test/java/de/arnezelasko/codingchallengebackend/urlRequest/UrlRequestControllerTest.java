package de.arnezelasko.codingchallengebackend.urlRequest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

class UrlRequestControllerTest {

  private MockMvc mockMvc;

  private UrlRequestController urlRequestController;

  @BeforeEach
  void setUp() {
    urlRequestController = new UrlRequestController();
    this.mockMvc = standaloneSetup(urlRequestController).build();
  }

  @Test
  void getUrlRequests() throws Exception {
    // given

    // when
    ResultActions resultActions =
        mockMvc.perform(get("/urlRequests").accept(MediaType.TEXT_HTML)).andDo(print());

    // then
    resultActions.andExpect(status().isOk());
  }
}
