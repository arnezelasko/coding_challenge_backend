package de.arnezelasko.codingchallengebackend;

import org.jeasy.random.EasyRandom;

public class EasyRandomUtil {

  private static final EasyRandom easyRandom = createEasyRandomInstance();

  public static <T> T getRandomInstance(Class<T> clazz) {
    return easyRandom.nextObject(clazz);
  }

  private static EasyRandom createEasyRandomInstance() {
    return new EasyRandom();
  }
}
