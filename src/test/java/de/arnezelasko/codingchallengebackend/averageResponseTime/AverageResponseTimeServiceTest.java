package de.arnezelasko.codingchallengebackend.averageResponseTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.assertj.core.util.Lists.newArrayList;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequest;
import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequestService;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

class AverageResponseTimeServiceTest {

  @Mock private UrlRequestService mockUrlRequestService;

  private AverageResponseTimeService averageResponseTimeServiceUnderTest;

  @BeforeEach
  void setUp() {
    initMocks(this);
    averageResponseTimeServiceUnderTest = new AverageResponseTimeService(mockUrlRequestService);
  }

  @Test
  void testGetAverageResponseTimesInMiliSecondsOrderByRequestTime() throws Exception {

    // given
    LocalDateTime firstRequestTime = LocalDateTime.of(2020, 4, 20, 20, 40, 0);
    List<Integer> requestDurationsInSeconds = newArrayList(2, 4, 3, 3, 4, 2);
    List<UrlRequest> urlRequests = createUrlRequests(firstRequestTime, requestDurationsInSeconds);
    long intervalInSeconds = 2L;
    givenUrlRequests(urlRequests);

    // when
    final List<AverageResponseTime> result =
        averageResponseTimeServiceUnderTest.getAverageResponseTimesInMiliSecondsOrderByRequestTime(
            intervalInSeconds);

    // then
    assertThat(result)
        .extracting("timeFrameLabel", "averageResponseTime")
        .containsExactly(
            tuple("20:40:00-20:40:02", 3000.0),
            tuple("20:40:02-20:40:04", 4000.0),
            tuple("20:40:04-20:40:06", 3000.0),
            tuple("20:40:06-20:40:08", 3000.0),
            tuple("20:40:08-20:40:10", 3000.0),
            tuple("20:40:10-20:40:12", 4000.0),
            tuple("20:40:12-20:40:14", 4000.0),
            tuple("20:40:14-20:40:16", 2000.0));
  }

  private List<UrlRequest> createUrlRequests(
      LocalDateTime firstRequestTime, List<Integer> durationInSeconds)
      throws MalformedURLException {
    LocalDateTime currentRequestTime = firstRequestTime;
    List<UrlRequest> urlRequests = new ArrayList<>();
    for (int i = 0; i < durationInSeconds.size(); i++) {
      LocalDateTime nextRequestTime = currentRequestTime.plusSeconds(durationInSeconds.get(i));
      urlRequests.add(createUrlRequest(currentRequestTime, nextRequestTime));
      currentRequestTime = nextRequestTime;
    }
    return urlRequests;
  }

  private UrlRequest createUrlRequest(LocalDateTime requestTime, LocalDateTime responseTime)
      throws MalformedURLException {
    return new UrlRequest(new URL("http://example.com/"), requestTime, responseTime);
  }

  private void givenUrlRequests(List<UrlRequest> urlRequests) {
    when(mockUrlRequestService.findAllOrderByRequestTime()).thenReturn(urlRequests);
  }
}
