package de.arnezelasko.codingchallengebackend.averageResponseTime;

import static java.time.LocalDateTime.of;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class AverageResponseTimeTest {

  @Test
  public void getTimeFrameLabelIsCorrect() {
    // given
    LocalDateTime timeFrameStart = of(2020, 4, 25, 18, 00);
    LocalDateTime timeFrameEnd = of(2020, 4, 25, 19, 00);
    double averageResponseTime = 10.0;
    AverageResponseTime averageResponseTimeInstance =
        new AverageResponseTime(timeFrameStart, timeFrameEnd, averageResponseTime);

    // when
    String timeFrameLabel = averageResponseTimeInstance.getTimeFrameLabel();

    // then
    assertThat(timeFrameLabel).isEqualTo("18:00:00-19:00:00");
  }
}
