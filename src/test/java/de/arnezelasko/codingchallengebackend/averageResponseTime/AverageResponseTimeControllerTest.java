package de.arnezelasko.codingchallengebackend.averageResponseTime;

import static java.time.LocalDateTime.of;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

class AverageResponseTimeControllerTest {

  private MockMvc mockMvc;

  private AverageResponseTimeService averageResponseTimeService;

  private AverageResponseTimeController averageResponseTimeController;

  @BeforeEach
  void setUp() {
    averageResponseTimeService = mock(AverageResponseTimeService.class);
    averageResponseTimeController = new AverageResponseTimeController(averageResponseTimeService);
    this.mockMvc = MockMvcBuilders.standaloneSetup(averageResponseTimeController).build();
  }

  @Test
  void getAverageResponseTimeReturnsMaxItemCount() throws Exception {
    // given
    long interValInSeconds = 1000;
    LocalDateTime firstTime = of(2020, 4, 25, 20, 21);
    LocalDateTime secondTime = of(2020, 4, 25, 20, 22);
    int maxItemCount = 1;
    List<AverageResponseTime> averageResponseTimes =
        createAverageResponseTime(newArrayList(firstTime, secondTime));
    givenAverageResponseTimes(interValInSeconds, averageResponseTimes);
    // when
    ResultActions resultActions =
        mockMvc
            .perform(
                get("/averageResponseTimes")
                    .param("intervallInSeconds", "" + interValInSeconds)
                    .param("maxItemCount", "" + maxItemCount)
                    .accept(MediaType.APPLICATION_JSON))
            .andDo(print());

    // then
    resultActions.andExpect(status().isOk());
    resultActions.andExpect(jsonPath("$", hasSize(maxItemCount)));
    resultActions.andExpect(jsonPath("$[0].timeFrameLabel", is("20:21:00-20:21:01")));
    resultActions.andExpect(jsonPath("$[0].averageResponseTime", is(1.0)));
  }

  private List<AverageResponseTime> createAverageResponseTime(List<LocalDateTime> timeFrameStarts) {
    List<AverageResponseTime> averageResponseTimes = new ArrayList<>();
    timeFrameStarts.stream()
        .forEach(
            timeFrameStart ->
                averageResponseTimes.add(
                    new AverageResponseTime(timeFrameStart, timeFrameStart.plusSeconds(1), 1.0)));
    return averageResponseTimes;
  }

  private void givenAverageResponseTimes(
      long interValInSeconds, List<AverageResponseTime> averageResponseTimes) {
    when(averageResponseTimeService.getAverageResponseTimesInMiliSecondsOrderByRequestTime(
            interValInSeconds))
        .thenReturn(averageResponseTimes);
  }
}
