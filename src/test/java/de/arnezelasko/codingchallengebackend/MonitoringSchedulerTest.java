package de.arnezelasko.codingchallengebackend;

import static java.lang.Long.parseLong;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

@SpringBootTest(classes = UrlRequestMonitoringApplication.class)
class MonitoringSchedulerTest {

  @SpyBean private MonitoringScheduler monitoringSchedulerUnderTest;

  private final long START_SCHEDULER_DELAY_TOLERANCE_MILI_SECONDS = 10;

  @Value("${sendRequestInterValInMiliSeconds}")
  String sendRequestInterValInMiliSeconds;

  @Test
  void testMonitorUrl() {
    // Setup

    // Run the test
    await()
        .atMost(
            parseLong(sendRequestInterValInMiliSeconds)
                + START_SCHEDULER_DELAY_TOLERANCE_MILI_SECONDS,
            MILLISECONDS)
        .untilAsserted(() -> verify(monitoringSchedulerUnderTest, atLeast(1)).monitorUrl());
  }
}
