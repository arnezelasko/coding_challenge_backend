package de.arnezelasko.codingchallengebackend;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequest;
import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequestExecutor;
import de.arnezelasko.codingchallengebackend.urlRequest.UrlRequestService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

@Slf4j
class MonitoringServiceTest {

  private static final String URL = "http://example.com/";

  @Mock private UrlRequestService urlRequestService;

  @Mock private UrlRequestExecutor urlRequestExecutor;

  private MonitoringService monitoringService;

  private ArgumentCaptor<UrlRequest> urlRequestArgumentCaptor = forClass(UrlRequest.class);

  @BeforeEach
  void setUp() throws MalformedURLException {
    initMocks(this);
    monitoringService = new MonitoringService(URL, urlRequestService, urlRequestExecutor);
  }

  @Test
  void createUrlRequestSuccessfully() throws Exception {
    // Setup
    givenSuccessfulUrlExecutionForUrl(URL);
    // Run the test
    monitoringService.monitorUrlRequest();

    // Verify the results

    verify(urlRequestService).saveUrlRequest(urlRequestArgumentCaptor.capture());
    UrlRequest urlRequest = urlRequestArgumentCaptor.getValue();
    assertThat(urlRequest.getRequestTime()).isNotNull();
    assertThat(urlRequest.getResponseTime()).isNotNull();
    assertThat(urlRequest.getResponseTime()).isAfterOrEqualTo(urlRequest.getRequestTime());
    assertThat(urlRequest.getUrl()).isEqualTo(new URL(URL));
  }

  private void givenSuccessfulUrlExecutionForUrl(String url)
      throws InterruptedException, IOException, URISyntaxException {
    doNothing().when(urlRequestExecutor).executeUrlRequest(eq(new URL(url)));
  }
}
